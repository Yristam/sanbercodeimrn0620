//soal if else
console.log("Soal IF ELSE");
console.log("");
var nama = "Jenita"
var peran = "Guard"

if(nama == "" && peran ==""){
    console.log("Nama harus diisi!");
}
if(nama == "John" && peran == ""){
    console.log("Halo " + nama +", Pilih peranmu untuk memulai game!");
}
if(nama == "Jane" && peran == "Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, " + nama );
    console.log("Halo " + peran +" " + nama +", kamu dapat melihat siapa yang menjadi werewolf!");
}    
if(nama == "Jenita" && peran == "Guard"){
    console.log("Selamat datang di Dunia Werewolf, " + nama );
    console.log("Halo " + peran +" " + nama +", kamu akan membantu melindungi temanmu dari serangan werewolf.");
}
if(nama == "Junaedi" && peran == "Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, " + nama );
    console.log("Halo " + peran +" " + nama +", kamu akan memakan mangsa setiap malam");
}
console.log("");
//switch case
console.log("Soal Switch case");
console.log("");

var tanggal = 1; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 3; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1945; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)



if (tanggal <1 || tanggal >31){
    console.log("tanggal error, harap masukan kembali dengan benar!");
}
else if (tahun <1900 || tahun >2200){
    console.log("tahun error, harap masukan kembali dengan benar!");
}
else{
    switch(bulan) {
        case 1:   { console.log(tanggal + ' Januari ' + tahun); break; }
        case 2:   { console.log(tanggal + ' Februari '+ tahun); break; }
        case 3:   { console.log(tanggal + ' Maret '+ tahun); break; }
        case 4:   { console.log(tanggal + ' April '+ tahun); break; }
        case 5:   { console.log(tanggal + ' Mei '+ tahun); break; }
        case 6:   { console.log(tanggal + ' Juni '+ tahun); break; }
        case 7:   { console.log(tanggal + ' Juli '+ tahun); break; }
        case 8:   { console.log(tanggal + ' Agustus '+ tahun); break; }
        case 9:   { console.log(tanggal + ' September '+ tahun); break; }
        case 10:   { console.log(tanggal + ' Oktober '+ tahun); break; }
        case 11:   { console.log(tanggal + ' November '+ tahun); break; }
        case 12:   { console.log(tanggal + ' Desember '+ tahun); break; }
        default:   {console.log('bulan tidak terdaftar');}
    }
}

