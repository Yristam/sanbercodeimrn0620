
//Arrow Functions
console.log("No 1 Arrow Functions");
console.log("");

  
var f = () => {
    //function
    console.log("this is golden!!")
    return() => {

    }
}
f();
console.log(" ");

//Object literal
console.log("No 2 Object literal");
console.log("");


const newFunction = function literal(firstName, lastName){
    return {
      firstName,
      lastName,
      fullName : function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 
  console.log(" ");


  //Destructuring
  console.log("No 3 Destructuring");
  console.log("");

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination, occupation, spell} = newObject;
// Driver code
console.log(firstName, lastName, destination, occupation)

console.log(" ");


//Array Spreading
console.log("No 4 Array Spreading");
console.log("");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//Driver Code
let combinedArray = [...west, ...east]
console.log(combinedArray)

console.log(" ");


//Template Literals
console.log("No 5 Template Literals");
console.log("");

const planet = "earth"
const view = "glass"
var before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit, ${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log(before)
