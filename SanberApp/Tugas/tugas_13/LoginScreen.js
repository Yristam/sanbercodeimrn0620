import React, { Component } from 'react';
import { View, Text,TextInput, StyleSheet, Image, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';



export default class App extends Component {
    state = {
        email: '',
        password: ''
     }
     handleEmail = (text) => {
        this.setState({ email: text })
     }
     handlePassword = (text) => {
        this.setState({ password: text })
     }
     login = (email, pass) => {
       alert('email: ' + email + ' password: ' + pass)
     }
render() {
    return (
        <View style = {styles.container}>
              <View style= {styles.top}>
              <Image source= {require('./logo.png')} style = {{width:400, height: 120}} />
              <Text style = {styles.text}>Login</Text>
              </View>
            <View style={styles.mid}>
                <Text style = {styles.text2}>Username/Email</Text>
                <TextInput style = {styles.input}
                underlineColorAndroid = "transparent"
                autoCapitalize = "none"
                onChangeText = {this.handleEmail}
                />
                <Text style = {styles.text2}>Password</Text>
                <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               autoCapitalize = "none"
               onChangeText = {this.handlePassword}/>

            </View>
                <View style={styles.bot}>

                </View>  
        </View>


      )
    }
};
const styles = StyleSheet.create({
    container : {
      flex: 1,
      paddingTop:25,

    },
    top :{
        
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 80
    },
    text: {
        fontSize: 24,
        color: '#003366',
        paddingTop:60
    },
    text2: {
        fontSize:16,
        color: '#003366',
        paddingTop:20,
        paddingHorizontal: 15
    },
    mid: {
        flex:1,
        justifyContent:"flex-start"
    },
    input: {
        margin: 10,
        height: 40,
        borderColor: '#003366',
        borderWidth: 1
    }

});