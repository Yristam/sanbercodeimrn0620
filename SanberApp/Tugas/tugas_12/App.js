import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Videoitem from './videoItem';
import data from './data.json';

export default class App extends Component {
  render() {
    return (
      <View style = {styles.container}>
        <View style = {styles.navBar}>
          <Image source = {require('./logo.png')} style = {{width:98, height: 18}}/>
          <View style = {styles.rightNav}>
            <TouchableOpacity><Icon style = {styles.navItem} name = 'search' size= {25}/>
            </TouchableOpacity>
            <TouchableOpacity>
            <Icon style = {styles.navItem} name = 'account-circle' size= {25}/>
            </TouchableOpacity>
          </View>
        </View>
        <View style= {styles.body}>
        <FlatList 
        data={data.items}
        renderItem={(video)=><Videoitem video={video.item} />}
        ItemSeparatorComponent={()=><View style={{height:0.5, backgroundColor: '#e5e5e5'}}/>}
        />


        </View>
        <View style = {styles.tabBar}>
          <TouchableOpacity style = {styles.tabItem}>
            <Icon name= 'home' size = {25}/>
            <Text style= {styles.tabTitle}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity style = {styles.tabItem}>
            <Icon name= 'explore' size = {25}/>
            <Text style= {styles.tabTitle}>Explore</Text>
          </TouchableOpacity>
          <TouchableOpacity style = {styles.tabItem}>
            <Icon name= 'subscriptions' size = {25}/>
            <Text style= {styles.tabTitle}>Subscriptions</Text>
          </TouchableOpacity>
          <TouchableOpacity style = {styles.tabItem}>
            <Icon name= 'email' size = {25}/>
            <Text style= {styles.tabTitle}>Inbox</Text>
          </TouchableOpacity>
          <TouchableOpacity style = {styles.tabItem}>
            <Icon name= 'folder' size = {25}/>
            <Text style= {styles.tabTitle}>Library</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container : {
    flex: 1,
  },
  navBar: {
    height: 70,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  rightNav: {
    flexDirection: 'row'
  },
  navItem:{
    marginLeft: 25
  },
  tabBar: {
    backgroundColor: 'white',
    height: 60,
    borderTopWidth: 1,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  body:{
    flex: 1
  },
  tabTitle: {
    fontSize: 11,
    color: 'black',
    paddingTop: 4
  },
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center'
  }

});
