// soal no 1 array range
console.log("No 1 array range");
console.log("");


function range(startnum, finishnum){
  var gg = [startnum];
  if(startnum<finishnum){
    for(startnum;startnum<finishnum;startnum++){
      gg.push(startnum+1);
    }
    return gg;
    
  }else if(startnum>finishnum){
    for(startnum;startnum>finishnum;startnum--){
      gg.push(startnum-1);
    }
    return gg;
  }else{
    return [-1];
  }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log("");
console.log(range(1)) // -1
console.log("");
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log("");
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log("");
console.log(range()) // -1

// soal no 2 array range with step
console.log("no 2 array range with step");
console.log("");

function rangeWithStep(z, x, c) {
  var q = [z];
  if(z<x){
    for(z;z<x-1;z+=c){
      q.push(z+c);
    }
    return q;
    
  }else if(z>x){
    for(z;z>x;z-=c){
      q.push(z-c);
    }
    return q;
  }
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]


