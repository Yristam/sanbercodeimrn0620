// soal no 1 looping while
console.log("No 1 Looping While");
console.log("");
console.log("LOOPING PERTAMA");


var angka = 2;

while(angka<=20){
    console.log(angka + ' - I love coding');
    angka +=2;
}
console.log("");
console.log("LOOPING KEDUA");

var angka2 = 20;

while(angka2>0){
    console.log(angka2 + ' - I will become a mobile developer');
    angka2 += -2;
}
// soal no 2 looping menggunakan for
console.log("");
console.log("No 2 looping menggunakan for");
console.log("");

for(var jumlah = 1;jumlah<=20;jumlah ++){
        if(jumlah%2==1 && jumlah%3){
            console.log(jumlah + ' - santai');
        }
        else if(jumlah%2==0){
            console.log(jumlah + ' - berkualitas');
        }
        else if(jumlah%3==0 &&jumlah%1==0){
            console.log(jumlah + ' - i love coding');
        }
}

// soal no 3 membuat persegi panjang
console.log("");
console.log("No 3 membuat persegi panjang");
console.log("");
var text = '#';
for( var box = 1; box <=7; box ++){
     text += '#';
}
for( var box2 =1; box2<=4;box2 ++){
    console.log(text);
}

// soal no 4 membuat tangga
console.log("");
console.log("no 4 membuat tangga");
console.log("");
var a = '#';
for( var tangga=1;tangga<=7;tangga++){
        console.log(a);
        a +='#';

}

// soal no 5 membuat papan catur
console.log("");
console.log("no 5 membuat papan catur");
console.log("");
var hitam = '#';
var putih = ' ';
var j ='';
for( var nilai=1;nilai<=8;nilai++){
    for( var nilai2=1;nilai2<=8;nilai2++){
        if(nilai%2==1){
            if(nilai2%2==0){
                j=j+putih;
            }
            else{
                j=j+hitam;
            }
        }
        else{
            if(nilai2%2==0){
                j=j+hitam;
            }
            else{
                j=j+putih;
            }
        }

    }
    console.log(j);
    j='';
}