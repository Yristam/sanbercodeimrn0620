console.log("")
console.log("Soal no 1 Array to Object")
console.log("")

var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function arrayToObject(arr){
    var i= 0;

    function age(){
        tahun = people[i][3]
        if( tahun === undefined || tahun > thisYear){
             return "Invalid birth Year";
        }else{
           d = thisYear - tahun ;
           return d;
        }

    }
    for(var i=0;i<arr.length;i++){
        var a= {
        firstname : people[i][0],
        lastname : people[i][1],
        gender : people[i][2],
        age : age()
        }
        console.log((i+1) + " " + people[i][0] + " " + people[i][1] + ": ", a);
    }   


}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
// Error case 
arrayToObject([]) // ""

console.log("")
console.log("Soal no 2 Shopping Time")
console.log("")

function shoppingTime(memberId, money){
   
    if(memberId === ''||memberId === undefined){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }else if(money<50000){
        return 'Mohon maaf, uang tidak cukup';
    }else{

          
       var barang= [''];
        var kembalian = money;
        for(i=0;i<=4;i++){
       if(kembalian>=1500000){
           kembalian -= 1500000;
           return barang.push[i] = 'Sepatu Stacattu';
           
       }else if(kembalian>=500000){
           kembalian -=500000;
           return barang.push[i] = 'Baju Zoro';
           
       }else if(kembalian>=250000){
           kembalian -=250000;
           return barang.push[i] = 'Bazu H&N';
       }else if(kembalian>=175000){
           kembalian -=175000;
           return barang.push[i] = 'Sweater Uniklooh';
       }else if(kembalian>=50000){
           kembalian -=50000;
           return barang.push[i] = 'Casing Handphone';
       }
       return barang;
      }

    }
     
     var k ={
        memberId : memberId,
        money : money,
        listPurchased : barang,
        changeMoney : kembalian

        }
        
     console.log(k);


}
 


 // TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja