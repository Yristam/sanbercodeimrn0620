////////////////////////////////////////////////////
console.log(" ");
console.log("No 1 Animal Class Release 0 ");
console.log(" ");
////////////////////////////////////////////////////
class Animal {
    // Code class di sini
    constructor (a){
        this.n = a;
        this.legs = 4;
        this.cold_blooded = "false";
    }
    get name(){
        return this.n;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

////////////////////////////////////////////////////
console.log(" ");
console.log("No 1 Animal Class Release 1 ");
console.log(" ");
////////////////////////////////////////////////////




// Code class Ape dan class Frog di sini
class Ape extends Animal{
    constructor(a, b){
        super(a);
        this.jns = b;
        this.legs = 2;
    }
    yell(){
        console.log("Auooo");
    }

}
class Frog extends Animal{
    constructor(a, c){
        super(a);
        this.jn = c;
    }
    jump(){
        console.log("hop hop");
    }

}

 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop"

////////////////////////////////////////////////////
console.log(" ");
console.log("No 2 Function to Class");
console.log(" ");
////////////////////////////////////////////////////


class Clock {
    // Code di sini
    constructor({ template }){
        this.time = template;

    }
    
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  