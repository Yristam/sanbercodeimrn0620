console.log(" ");
console.log('soal A');
console.log(" ");

function DescendingTen(num){
    if(num !== undefined){
        var c=[];
        for(i=num;i>0;i--){
            c.push(i);
        }
        return c.toString();
    }else{
        return -1;
    }
}
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1

console.log(" ");
console.log('soal B');
console.log(" ");

function AscendingTen(num){
    if(num !== undefined){
        var c=[];
        for(i=num;i<num+10;i++){
            c.push(i);
        }
        return c.toString();
    }else{
        return -1;
    }
}
// TEST CASES Ascending Ten
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1