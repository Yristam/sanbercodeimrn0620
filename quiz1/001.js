console.log(" ");
console.log('soal A');
console.log(" ");


function bandingkan(num1, num2){

    if(num1 >= 0 && num2 >= 0){
        if(num1>num2){
            return num1;
        }else if(num1<num2){
            return num2;
        }else{
            return -1;
        }
    }else {
        return -1;
    }

}

console.log(bandingkan(5,10));
console.log(bandingkan(15,10));
console.log(bandingkan(10,10));
console.log(bandingkan(-5,-10));
console.log(bandingkan(-5,10));
console.log(bandingkan(5,-10));

console.log(" ");
console.log('soal B');
console.log(" ");


function balikstring(str){
    var huruf = str;
    var newhuruf = '';
    for(var i= str.length-1; i>=0 ; i--){
        newhuruf = newhuruf + huruf[i];
    }
    return newhuruf;
    
}
console.log(balikstring("japanase"));

console.log(" ");
console.log('soal C');
console.log(" ");

function palindrome(a){
    var b = a.length;
    for (var j= 0;j<b/2;j++){
        if(a[j] !== a[b - 1 - j ]){
            return false;
        }
    }
    return true;
}
console.log(palindrome("mama"));
console.log(palindrome("radar"));